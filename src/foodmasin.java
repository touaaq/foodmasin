import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodmasin {
    private JButton tempraButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton yakisobaButton;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JPanel root;
    private JLabel priceLabel;
    private JTextArea textArea1;
    private JTextPane noOrderReceivedTextPane;



    //食品ボタンを押した時の処理
    void botan(String food,int num){
        int confirmation= JOptionPane.showConfirmDialog(null, "Would you like to order "+food+" ?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
        //はいを選択した時
        if(confirmation==0){
            //ダイアログを出す
            JOptionPane.showMessageDialog(null,"Thank you for " +
                    "ordering " +food+ "! it will be served as soon as possible");
            num=karaage.sum+=100;

            //右の空欄に注文した食品を表示し改行する append:文字列を連結する
            textArea1.append(food);
            textArea1.append("\n");
            //合計金額を表示する, String.valueof()で整数を表示できる
            priceLabel.setText("total        "+String.valueOf(num)+"yen");

        }

    }
    //クラス間共有変数sumを宣言
    public class karaage{
        public static int sum=0;
    }

    public foodmasin() {


        tempraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               botan("Tempra",karaage.sum);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Karaage",karaage.sum);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Gyoza",karaage.sum);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Udon",karaage.sum);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Yakisoba",karaage.sum);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                botan("Ramen",karaage.sum);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation= JOptionPane.showConfirmDialog(null, "Would you like to checkout ?", "Message", JOptionPane.YES_NO_OPTION);
                //はいを選択したとき
                if(confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you.The total price is "+karaage.sum+" yen");
                    //注文した食品名を消す
                    textArea1.setText(null);
                    //total 0yenに戻す
                    priceLabel.setText("total        0yen");
                    //sumを０に初期化
                    karaage.sum=0;
                }
            }
        });
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("foodmasin");
        frame.setContentPane(new foodmasin().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

